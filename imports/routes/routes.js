import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory} from 'react-router';
import Login from '../ui/Login';
import Signup from '../ui/Signup';
import Dashboard from '../ui/Dashboard';
import NotFound from '../ui/NotFound';

const authenticatedPages = ['/dashboard'];
const unauthenticatedPages = ['/', '/signup'];


onEnterPublicPages = () => {
  Meteor.userId() && browserHistory.replace('/dashboard');
};

onEnterPrivatePages = () => {
  !Meteor.userId() && browserHistory.replace('/')
};

export const onAuthChange = (isAuthenticated) => {
    const pathname = browserHistory.getCurrentLocation().pathname;
    const isAuthenticatedPage = authenticatedPages.includes(pathname);
    const isUnauthenticatedPage = unauthenticatedPages.includes(pathname);

    if(isUnauthenticatedPage && isAuthenticated) {
    browserHistory.replace('/dashboard');
    } else if (isAuthenticatedPage && !isAuthenticated) {
    browserHistory.replace('/')
    }
};


export const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={Login} onEnter={this.onEnterPublicPages}/>
    <Route path="/signup" component={Signup} onEnter={this.onEnterPublicPages}/>
    <Route path="/dashboard" component={Dashboard} onEnter={this.onEnterPrivatePages}/>
    <Route path="*" component={NotFound} />
  </Router>
);